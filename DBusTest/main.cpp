#include <QtDBus>
#include <QDebug>

#include "sinkinterface.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QDBusInterface remoteApp1("org.PulseAudio1", "/org/pulseaudio/server_lookup1", "org.freedesktop.DBus.Properties");
    QDBusReply<QDBusVariant> reply = remoteApp1.call("Get", "org.PulseAudio.ServerLookup1", "Address");
    QString socketAddress = reply.value().variant().toString();

    //QDBusConnection conn("pulseaudio");
    QDBusConnection conn = QDBusConnection::connectToPeer(socketAddress, "pulseaudio");

    qDebug() << conn.name() << conn.isConnected();
    qDebug() << conn.lastError();
    //qDebug() << QDBusConnection::sessionBus().name();

    QDBusInterface iface("org.PulseAudio.Core1", "/org/pulseaudio/core1", "org.freedesktop.DBus.Properties",
                         conn);

    qDebug() << iface.lastError();
    QDBusMessage replMessage = iface.call("Get", "org.PulseAudio.Core1", "Sinks");
    qDebug() << replMessage;
    QVariant tempVar = replMessage.arguments().first();
    tempVar = tempVar.value<QDBusVariant>().variant();
    QDBusArgument dbusArgs = tempVar.value<QDBusArgument>();
    QDBusObjectPath sinkPath;
    dbusArgs.beginArray();
    while (!dbusArgs.atEnd())
    {
        dbusArgs >> sinkPath;
        // append path to a vector here if you want to keep it
    }
    dbusArgs.endArray();

    OrgPulseAudioCore1DeviceInterface devIface("org.PulseAudio.Core1.Device", sinkPath.path(), conn);

    qDebug() << devIface.lastError();
    devIface.setMute(!devIface.mute());


    //QDBusInterface sinkIface("org.PulseAudio.Core1.Device", sinkPath.path(), "org.freedesktop.DBus.Properties", conn);
    //replMessage = sinkIface.call("Get", "org.PulseAudio.Core1.Device", "Mute");
    //qDebug() << replMessage;
    //bool muteState = replMessage.arguments().first().toBool();

    //sinkIface.call("Set", "org.PulseAudio.Core1.Device", "Mute", QVariant::fromValue(!muteState));
    //qDebug() << sinkIface.lastError();

    conn.disconnectFromPeer("pulseaudio");
}

