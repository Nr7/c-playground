#-------------------------------------------------
#
# Project created by QtCreator 2014-02-01T17:14:24
#
#-------------------------------------------------

QT       += core\
            dbus

QT       -= gui

TARGET = DBusTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    sinkinterface.cpp

HEADERS += \
    sinkinterface.h

OTHER_FILES += \
    sink.xml
